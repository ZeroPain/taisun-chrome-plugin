// Saves endpoint to local storage
function save_options() {
  var endpoint = document.getElementById('endpoint').value;
  var message = document.getElementById('message');
  // Clear message on load
  message.innerHTML = "";
  // If this is a legit taisun.io endpoint save it
  if (RegExp("(.*).taisun.io:(.*)").test(endpoint)){
    localStorage['endpoint'] = endpoint;
    message.append('Options saved.');
    setTimeout(function() {
      message.innerHTML = "";
    }, 1500);
  }
  // If validation failed flash error
  else {
    message.append('Invalid format: required yourdomain.taisun.io:yourport');
    setTimeout(function() {
      message.innerHTML = "";
    }, 1500);
  }
}

// Populate the form with the saved settings
function restore_options() {
  var endpoint = localStorage['endpoint'];
  if (typeof endpoint !== 'undefined' && endpoint !== null){
    document.getElementById('endpoint').value = endpoint;
  }
}
document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('saveproxy').addEventListener('click',save_options);
